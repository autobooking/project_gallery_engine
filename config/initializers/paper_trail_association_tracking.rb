# frozen_string_literal: true

# link to rep on github
#https://github.com/westonganger/paper_trail-association_tracking/blob/be4913a30c1e47d540038e3504e0647098b56ba0/lib/paper_trail_association_tracking/reifier.rb

module PaperTrailAssociationTracking
  # Given a version record and some options, builds a new model object.
  # @api private
  module Reifier
    module ClassMethods

      # Restore the `model`'s has_many associations as they were at version_at
      # timestamp We lookup the first child versions after version_at timestamp or
      # in same transaction.
      # @api private
      def reify_has_manys(transaction_id, model, options = {})
        assoc_has_many_through, assoc_has_many_directly =
            model.class.reflect_on_all_associations(:has_many).
                partition { |assoc| assoc.options[:through] }

        reify_has_many_associations(transaction_id, assoc_has_many_directly, model, options)
        # reject station association because of has_many through has_one
        reify_has_many_through_associations(transaction_id, assoc_has_many_through.reject{|r| r.options[:through] == :station}, model, options)
      end
    end
  end
end
