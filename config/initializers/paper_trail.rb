PaperTrail.config.track_associations = true

PaperTrail.config.association_reify_error_behaviour = :warn

PaperTrail.config.has_paper_trail_defaults = {
    on: %i[create update]
}
