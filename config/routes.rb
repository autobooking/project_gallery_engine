Rails.application.routes.draw do
  resources :projects

  namespace :station_admin do
    resources :projects, only: [:index, :edit, :update, :destroy]

    namespace :stations, path: nil do
      resources :stations, only: [] do
        resources :projects do
          resources :services, only: [:index] do
            collection do
              put :update
            end
          end
          member do
            patch :cancel_approve, to: 'projects/versions#cancel_approve'
          end
        end

        resources :project_photos, only: [:create, :update, :destroy] do
          post :sort, on: :collection
        end
      end
    end
  end

  namespace :api, format: :json do
    namespace :front do
      namespace :v2 do
        resources :projects, only: [:index, :show]

        namespace :projects_search do
          root action: :index

          get 'filters', action: :get_filters
        end

        resources :stations, only: [] do
          resources :projects, only: [:index], controller: 'stations/projects'
        end
      end
    end
  end
end
