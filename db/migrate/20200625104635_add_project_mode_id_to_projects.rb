class AddProjectModeIdToProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :project_mode_id, :integer, index: true, default: 1
  end
end
