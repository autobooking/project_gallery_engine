class AddNameToProjectPhotos < ActiveRecord::Migration[6.0]
  def change
    add_column :project_photos, :name, :string
  end
end
