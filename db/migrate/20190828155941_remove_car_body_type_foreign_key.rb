# frozen_string_literal: true

class RemoveCarBodyTypeForeignKey < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :projects, :car_body_types
  end
end
