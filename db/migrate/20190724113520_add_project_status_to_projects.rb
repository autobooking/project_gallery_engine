# frozen_string_literal: true

class AddProjectStatusToProjects < ActiveRecord::Migration[5.2]
  def change
    add_reference :projects, :project_status
  end
end
