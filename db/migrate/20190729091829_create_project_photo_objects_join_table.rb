# frozen_string_literal: true

class CreateProjectPhotoObjectsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :project_photos, :objects

    rename_table :objects_project_photos, :project_photo_objects
  end
end
