class CreateProjectModes < ActiveRecord::Migration[6.0]
  def change
    create_table :project_modes do |t|
      t.string :code, unique: true

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        ProjectMode.create_translation_table! name: :string, description: :text
      end

      dir.down do
        ProjectMode.drop_translation_table!
      end
    end
  end
end
