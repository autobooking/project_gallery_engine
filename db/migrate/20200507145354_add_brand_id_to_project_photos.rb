class AddBrandIdToProjectPhotos < ActiveRecord::Migration[6.0]
  def change
    add_reference :project_photos, :brand, foreign_key: true
  end
end
