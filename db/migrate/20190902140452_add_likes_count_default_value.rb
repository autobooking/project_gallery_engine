# frozen_string_literal: true

class AddLikesCountDefaultValue < ActiveRecord::Migration[5.2]
  def change
    change_column_default :projects, :likes_count, 0
  end
end
