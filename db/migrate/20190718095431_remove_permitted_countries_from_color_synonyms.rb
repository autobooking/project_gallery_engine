# frozen_string_literal: true

class RemovePermittedCountriesFromColorSynonyms < ActiveRecord::Migration[5.2]
  def change
    remove_column :color_synonyms, :permitted_countries, :text
  end
end
