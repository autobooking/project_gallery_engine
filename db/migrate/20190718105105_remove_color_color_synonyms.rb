# frozen_string_literal: true

class RemoveColorColorSynonyms < ActiveRecord::Migration[5.2]
  def change
    drop_table :color_color_synonyms
  end
end
