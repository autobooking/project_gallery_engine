# frozen_string_literal: true

class CreateColors < ActiveRecord::Migration[5.2]
  def change
    create_table :colors do |t|
      t.string :name
      t.string :slug
      t.string :image
      t.text   :permitted_countries, array: true, default: []

      t.timestamps

      t.index :slug
    end
  end
end
