# frozen_string_literal: true

class AddFootageToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :footage, :decimal, default: 0.0
  end
end
