# frozen_string_literal: true

class AddColorToProjects < ActiveRecord::Migration[5.2]
  def change
    add_reference :projects, :color
  end
end
