# frozen_string_literal: true

class RemoveCarBodyTypeFromProjects < ActiveRecord::Migration[5.2]
  def change
    remove_reference :projects, :car_body_type
  end
end
