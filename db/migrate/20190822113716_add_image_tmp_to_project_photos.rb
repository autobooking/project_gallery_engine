# frozen_string_literal: true

class AddImageTmpToProjectPhotos < ActiveRecord::Migration[5.2]
  def change
    add_column :project_photos, :image_tmp, :string
  end
end
