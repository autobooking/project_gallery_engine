# frozen_string_literal: true

class CreateMetaDatas < ActiveRecord::Migration[5.2]
  def change
    create_table :meta_datas do |t|
      t.string :title
      t.text :description
      t.text :keywords

      t.references :project, foreign_key: true
    end
  end
end
