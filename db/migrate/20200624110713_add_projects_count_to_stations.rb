class AddProjectsCountToStations < ActiveRecord::Migration[6.0]
  def up
    return unless table_exists?(:stations)

    return if column_exists? :stations, :projects_count

    add_column :stations, :projects_count, :integer, default: 0
  end
end
