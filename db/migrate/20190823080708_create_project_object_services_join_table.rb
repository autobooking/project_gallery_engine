# frozen_string_literal: true

class CreateProjectObjectServicesJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_table :project_object_services do |t|
      t.references :project, foreign_key: true
      t.references :object,  foreign_key: true
      t.references :service, foreign_key: true
    end
  end
end
