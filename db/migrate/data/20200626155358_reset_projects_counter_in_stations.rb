class ResetProjectsCounterInStations < ActiveRecord::Migration[6.0]
  def up
    return unless defined?(Station)

    Station.all.each do |s|
      Station.reset_counters(s.id, :projects)
    end
  end
end
