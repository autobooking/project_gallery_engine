# frozen_string_literal: true

class AddBrandToProjectPhotos < ActiveRecord::Migration[5.2]
  def change
    add_reference :project_photos, :brand
  end
end
