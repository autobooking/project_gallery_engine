# frozen_string_literal: true

class CreateColorSynonyms < ActiveRecord::Migration[5.2]
  def change
    create_table :color_synonyms do |t|
      t.string :name
      t.text :permitted_countries, default: [], array: true

      t.timestamps

      t.references :color, foreign_key: true
    end
  end
end
