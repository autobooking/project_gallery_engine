# frozen_string_literal: true

class AddIdToJoinTables < ActiveRecord::Migration[5.2]
  def change
    add_column :project_additional_services, :id, :primary_key
    add_column :project_car_body_types, :id, :primary_key
    add_column :project_photo_objects, :id, :primary_key
  end
end
