# frozen_string_literal: true

class CreateProjectVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :project_videos do |t|
      t.string :link

      t.references :project, foreign_key: true
    end
  end
end
