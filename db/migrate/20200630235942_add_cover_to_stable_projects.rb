class AddCoverToStableProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :stable_projects, :cover, :string
  end
end
