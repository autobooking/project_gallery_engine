# frozen_string_literal: true

class CreateColorsColorSynonymsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :colors, :color_synonyms
  end
end
