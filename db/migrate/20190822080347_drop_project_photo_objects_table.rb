# frozen_string_literal: true

class DropProjectPhotoObjectsTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :project_photo_objects
  end
end
