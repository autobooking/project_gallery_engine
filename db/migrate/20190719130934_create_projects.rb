# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.string :slug
      t.string :cover
      t.string :country
      t.string :address_real
      t.float :latitude
      t.float :longitude
      t.integer :views_count
      t.integer :likes_count
      t.date :end_date

      t.timestamps

      t.references :city
      t.references :style
      t.references :brand
      t.references :car_body_type
    end
  end
end
