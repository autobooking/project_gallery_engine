class CreateProjectPhotoObjectServices < ActiveRecord::Migration[6.0]
  def change
    create_table :project_photo_object_services do |t|
      t.references :project_photo, foreign_key: true
      t.references :object,  foreign_key: true
      t.references :service, foreign_key: true
    end
  end
end
