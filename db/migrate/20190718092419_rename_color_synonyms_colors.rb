# frozen_string_literal: true

class RenameColorSynonymsColors < ActiveRecord::Migration[5.2]
  def change
    rename_table :color_synonyms_colors, :color_color_synonyms
  end
end
