class AddAltToProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :alt, :string
  end
end
