# frozen_string_literal: true

class AddRegionToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :region, :string
  end
end
