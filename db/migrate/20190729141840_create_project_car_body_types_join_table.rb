# frozen_string_literal: true

class CreateProjectCarBodyTypesJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :projects, :car_body_types

    rename_table :car_body_types_projects, :project_car_body_types
  end
end
