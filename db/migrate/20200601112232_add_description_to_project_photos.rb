class AddDescriptionToProjectPhotos < ActiveRecord::Migration[6.0]
  def change
    add_column :project_photos, :description, :text
  end
end
