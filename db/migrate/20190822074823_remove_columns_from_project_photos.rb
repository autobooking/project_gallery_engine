# frozen_string_literal: true

class RemoveColumnsFromProjectPhotos < ActiveRecord::Migration[5.2]
  def change
    remove_column :project_photos, :imageable_id
    remove_column :project_photos, :imageable_type
    remove_column :project_photos, :author
    remove_column :project_photos, :description
    remove_column :project_photos, :brand_id
    remove_column :project_photos, :color_id
    remove_column :project_photos, :style_id
  end
end
