# frozen_string_literal: true

class AddNeedModerationToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :need_moderation, :boolean, default: true
  end
end
