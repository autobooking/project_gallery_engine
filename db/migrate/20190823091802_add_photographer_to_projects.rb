# frozen_string_literal: true

class AddPhotographerToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :photographer, :string
  end
end
