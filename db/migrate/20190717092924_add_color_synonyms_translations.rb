# frozen_string_literal: true

class AddColorSynonymsTranslations < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        ColorSynonym.create_translation_table!(name: :string)
      end

      dir.down do
        ColorSynonym.drop_translation_table!
      end
    end
  end
end
