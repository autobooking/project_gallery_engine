class AddStyleIdToProjectPhotos < ActiveRecord::Migration[6.0]
  def change
    add_column :project_photos, :style_id, :integer
  end
end
