# frozen_string_literal: true

class AddCarBodyTypeReferenceToProjects < ActiveRecord::Migration[5.2]
  def change
    add_reference :projects, :car_body_type, foreign_key: true
  end
end
