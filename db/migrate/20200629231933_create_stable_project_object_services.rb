class CreateStableProjectObjectServices < ActiveRecord::Migration[6.0]
  def change
    create_table :stable_project_object_services do |t|
      t.references :stable_project, foreign_key: true
      t.references :object,  foreign_key: true
      t.references :service, foreign_key: true
    end
  end
end
