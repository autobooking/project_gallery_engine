# frozen_string_literal: true

class RemoveCoverFromProjects < ActiveRecord::Migration[5.2]
  def change
    remove_column :projects, :cover
  end
end
