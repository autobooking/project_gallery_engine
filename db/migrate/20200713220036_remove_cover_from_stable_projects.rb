class RemoveCoverFromStableProjects < ActiveRecord::Migration[6.0]
  def change
    remove_column :stable_projects, :cover
  end
end
