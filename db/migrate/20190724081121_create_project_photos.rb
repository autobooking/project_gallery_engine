# frozen_string_literal: true

class CreateProjectPhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :project_photos do |t|
      t.string :image
      t.integer :position
      t.string :author
      t.text :description

      t.references :imageable, polymorphic: true
      t.references :style
      t.references :color

      t.timestamps
    end
  end
end
