class CreateStableProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :stable_projects do |t|
      t.string :name
      t.text :description
      t.string :slug
      t.string :country
      t.string :address_real
      t.string :photographer
      t.string :region
      t.string :alt
      t.float :latitude
      t.float :longitude
      t.integer :views_count
      t.integer :likes_count
      t.integer :car_body_type_id, index: true
      t.date :end_date
      t.decimal :footage, default: 0.0

      t.timestamps

      t.references :project_mode
      t.references :city
      t.references :style
      t.references :brand
      t.references :color
      t.references :project_status
      t.references :station
      t.references :project, foreign_key: true
    end
  end
end
