# frozen_string_literal: true

class AddAltToProjectPhotos < ActiveRecord::Migration[5.2]
  def change
    add_column :project_photos, :alt, :string
  end
end
