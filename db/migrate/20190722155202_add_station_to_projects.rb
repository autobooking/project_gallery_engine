# frozen_string_literal: true

class AddStationToProjects < ActiveRecord::Migration[5.2]
  def change
    add_reference :projects, :station
  end
end
