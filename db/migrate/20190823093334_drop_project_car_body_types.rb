# frozen_string_literal: true

class DropProjectCarBodyTypes < ActiveRecord::Migration[5.2]
  def change
    drop_table :project_car_body_types
  end
end
