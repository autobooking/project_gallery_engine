class CreateProjectStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :project_statuses do |t|
      t.hstore :name
    end

    reversible do |dir|
      dir.up do
        ProjectStatus.create_translation_table! name: :string
      end

      dir.down do
        ProjectStatus.drop_translation_table!
      end
    end
  end
end
