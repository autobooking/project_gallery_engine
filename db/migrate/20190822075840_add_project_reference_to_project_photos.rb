# frozen_string_literal: true

class AddProjectReferenceToProjectPhotos < ActiveRecord::Migration[5.2]
  def change
    add_reference :project_photos, :project
  end
end
