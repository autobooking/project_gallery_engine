# frozen_string_literal: true

class AddColorTranslations < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Color.create_translation_table!(name: :string, slug: :string)
      end

      dir.down do
        Color.drop_translation_table!
      end
    end
  end
end
