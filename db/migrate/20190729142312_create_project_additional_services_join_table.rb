# frozen_string_literal: true

class CreateProjectAdditionalServicesJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :projects, :additional_services

    rename_table :additional_services_projects, :project_additional_services
  end
end
