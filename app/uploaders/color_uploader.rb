# frozen_string_literal: true

require 'carrierwave'

class ColorUploader < BaseUploader
  process resize_to_limit: [100, 100]

  version :small do
    process resize_to_fit: [200, 50]
  end
end
