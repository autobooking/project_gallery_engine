# frozen_string_literal: true

require 'carrierwave'

class ProjectCoverUploader < BaseUploader
  process resize_to_limit: [3840, 2160]

  version :desktop_project_cover do
    process resize_to_fit: [1920, 1080]
  end

  version :mobile_project_cover do
    process resize_to_fit: [720, 480]
  end
end
