# frozen_string_literal: true

require 'carrierwave'

class ProjectPhotoUploader < BaseUploader
  process resize_to_limit: [3840, 2160]

  version :desktop_project_photo do
    process resize_to_fit: [1920, 1080]
  end

  version :mobile_project_photo do
    process resize_to_fit: [720, 480]
  end

  version :big_project_photo do
    process resize_to_fit: [800, 400]
  end

  version :medium_project_photo do
    process resize_to_fit: [450, 450]
  end
end
