# frozen_string_literal: true

module StationAdmin
  module Stations
    module Projects
      class ServicesController < BaseController
        before_action :station, only: [:index]
        before_action :project, only: [:index, :update]


        def index
          params[:display_menu_for_project] = true

          @project_brand_services = Service
                                      .with_preload_translations
                                      .brand_services
        end

        # TODO: Do refactor!
        def update
          destroy_services if resource_params[:destroy_brand_service_ids].present?

          resource_params[:service_ids].each do |service_id|
            project.project_object_services.create(service_id: service_id)
          end

          head :ok
        end

        private

        def resource_params
          params.require(:project).permit(service_ids: [], destroy_brand_service_ids: [])
        end

        def project
          @project = Project.find(params[:project_id])
        end

        def station
          @station = Station.find(params[:station_id])
        end

        def destroy_services
          destroy_brand_service_ids = resource_params[:destroy_brand_service_ids].first.split(',')

          if destroy_brand_service_ids.any?
            ProjectObjectService.where(service_id: destroy_brand_service_ids).update(service_id: nil)
          end
        end
      end
    end
  end
end
