# frozen_string_literal: true

module StationAdmin
  module Stations
    module Projects
      class VersionsController < BaseController
        before_action :station, :project, only: [:update]

        def cancel_approve
          ProjectVersionReview.new(project, resource_params).call

          redirect_to station_admin_stations_station_projects_path(station)
        end

        private

        def resource_params
          params.require(:project).permit(
            :name, :description, :photographer, :footage,
            meta_data_attributes: [:id, :title, :keywords, :description],
            project_photos_attributes: [:id, :image, :alt]
          )
        end

        def project
          @project ||= Project.find(params[:id])
        end

        def station
          @station ||= Station.find(params[:station_id])
        end

        def canceled?
          params[:commit] == 'cancel'
        end
      end
    end
  end
end
