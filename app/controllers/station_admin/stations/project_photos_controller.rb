# frozen_string_literal: true

module StationAdmin
  module Stations
    class ProjectPhotosController < BaseController
      def create
        @project_photo = ProjectPhoto.create(resource_params)
      end

      def destroy
        project_photo.destroy
      end

      def sort
        params[:project_photo].each.with_index(1) do |id, index|
          ProjectPhoto.update(id, position: index)
        end

        head :ok
      end

      private

      def resource_params
        params.require(:project_photo).permit(:image)
      end

      def project_photo
        @project_photo = ProjectPhoto.find(params[:id])
      end
    end
  end
end