# frozen_string_literal: true

module StationAdmin
  module Stations
    class ProjectsController < BaseController
      before_action :station, only: [:new, :create, :edit, :update, :index]
      before_action :project, only: [:edit, :update, :destroy]
      before_action :load_form_data, only: [:new, :edit]

      # set stable update event and leave one stable version on operator update
      before_action :set_paper_trail_event, only: [:update]
      after_action :clear_stable_versions, only: [:update]

      def new
        @project = station.projects.build

        @project.project_object_services.build

        @project.project_photos.build

        @project.project_videos.build

        @project.build_meta_data
      end

      def edit
        params[:display_menu_for_project] = true
      end

      def index
        @active_tab = 'station_projects'

        params[:display_menu_for_station] = true

        @projects = station.projects

        respond_to do |format|
          format.html
          format.json do
            render json: StationProjectDatatable.new(params,
                                                     station: station,
                                                     view_context: view_context)
          end
        end
      end

      def create
        attributes = resource_params.except(:project_photos_attributes).except(:city).merge(city_id: city_id)

        attributes.merge!(need_moderation: false) if current_user.operator? || current_user.station_manager?

        project = station.projects.create(attributes)

        ProjectPhoto.where(id: project_photo_ids).update(project_id: project.id)

        project.update(project_photo_params)
        flash[:notice] = I18n.t('project_gallery.station_admin.draft_project.created') unless current_user.operator? || current_user.station_manager?

        redirect_to station_admin_stations_station_projects_path(station)
      end

      def update
        attributes = resource_params.except(:city).merge(city_id: city_id)

        ProjectPhoto.where(id: project_photo_ids).update(project_id: project.id)

        if current_user.operator? || current_user.station_manager?
          if params[:commit] == 'update'
            @project.update(attributes)

            @project.update_stable_project
          else
            ProjectVersionReview.new(@project, params[:commit], attributes).call
          end

          if @project.errors.messages[:cover_image].present?
            flash[:error] = @project.errors.messages.values.flatten.join(', ')

            redirect_to edit_station_admin_stations_station_project_path(@project.station_id, @project.id, moderation: @project.need_moderation)
          else
            redirect_to station_admin_projects_path(in_my_country: true)
          end
        else
          @project.update(attributes.merge(need_moderation: true))
          flash[:notice] = I18n.t('project_gallery.station_admin.draft_project.created')

          @project.notify_about_project_review

          if @project.errors.messages[:cover_image].present?
            flash[:error] = @project.errors.messages.values.flatten.join(', ')

            redirect_to edit_station_admin_stations_station_project_path(@project.station_id, @project.id)
          else
            redirect_to station_admin_stations_station_projects_path(station)
          end
        end
      end

      def destroy
        station.projects.destroy(project)

        redirect_to station_admin_stations_station_projects_path(station)
      end

      private
        def set_active_tab
          @active_tab = 'project_moderation'
        end

        def resource_params
          params.require(:project).permit(
            # General
            :name, :style_id, :brand_id, :project_status_id, :photographer, :footage, :end_date, :alt, :cover,
            :project_mode_id,
            # Position
            :address_real, :region, :city, :country, :latitude, :longitude,
            # Description
            :description, additional_service_ids: [],
            # Meta data
            meta_data_attributes: [:title, :keywords, :description],
            # Interior objects
            project_object_services_attributes: [:id, :project_id, :object_id, :service_id, :_destroy],
            # Gallery
            project_photos_attributes: [:id, :alt, :name, :style_id, :brand_id, :description,
                                        project_photo_object_services_attributes: [:id, :project_photo_id, :object_id, :service_id, :_destroy]],
            # Videos
            project_videos_attributes: [:id, :link, :_destroy]
          )
        end

        def project_photo_params
          params.require(:project).permit(project_photos_attributes: [:id, :alt, :name, :style_id, :brand_id, :description,
                                                                      project_photo_object_services_attributes: [:id, :project_photo_id, :object_id, :service_id, :_destroy]])
        end

        def project
          project = Project.find(params[:id])
          if current_user.station_admin? || params[:moderation] == 'true'
            @project = project
          elsif current_user.operator? || current_user.station_manager?
            @project = project.stable_or_last_version
          end
        end

        def station
          @station ||= Station.find(params[:station_id])
        end

        def load_form_data
          @styles = Style.with_preload_translations if defined?(HallwayStyles::VERSION) && Settings.use_styles

          @brands = Brand.with_preload_translations

          @brand_services = Service.brand_services

          @colors = Color.with_preload_translations

          @objects = Catalog::Object.with_preload_translations

          @project_statuses = ProjectStatus.with_preload_translations

          @additional_services = AdditionalService.with_translations_by_order_id

          @car_body_types = CarBodyType.all
        end

        def project_photo_ids
          project_photo_params[:project_photos_attributes].to_h.values.map { |h| h['id'] }
        end

        def city_id
          City.find_by(name: resource_params[:city]).try(:id)
        end

        def clear_stable_versions
          return unless current_user.operator?

          stable_versions = @project.versions.where(event: Project::STABLE_VERSION_EVENT)

          return unless stable_versions.count > 1

          stable_versions.first.destroy
        end

        def set_paper_trail_event
          return unless current_user.operator?

          @project.paper_trail_event = Project::STABLE_VERSION_EVENT
        end
    end
  end
end
