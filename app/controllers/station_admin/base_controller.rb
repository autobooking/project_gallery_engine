# frozen_string_literal: true

if !Object.const_defined?('StationAdmin::BaseController')
  module StationAdmin
    class BaseController < ActionController::Base
      layout 'application'
    end
  end
end
