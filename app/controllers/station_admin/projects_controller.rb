# frozen_string_literal: true

module StationAdmin
  class ProjectsController < BaseController
    def index
      if current_user.operator? || current_user.station_admin?
        @active_tab = 'projects'

        session[:in_my_country] = params[:in_my_country]

        respond_to do |format|
          format.html
          format.json do
            render json: ProjectDatatable.new(params,
                                              view_context: view_context,
                                              in_my_country: params[:in_my_country],
                                              current_user: current_user)
          end
        end
      else
        redirect_to station_admin_root_url
      end
    end

    def edit
      @active_tab = 'projects'
    end

    def update
      project.update!(resource_params)

      redirect_to edit_station_admin_project_path(project)
    end

    def destroy
      project.destroy

      redirect_to station_admin_projects_path(anchor: 'projects', in_my_country: session[:in_my_country])
    end

    private

    def project
      @project ||= Project.find(params[:id])
    end

    def resource_params
      params.require(:project).permit(:name,
                                      :project_mode_id,
                                      :description,
                                      :cover,
                                      :country,
                                      :address_real,
                                      :city,
                                      :latitude, :longitude,
                                      :end_date)

    end
  end
end
