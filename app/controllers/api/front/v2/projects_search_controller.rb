# frozen_string_literal: true

module Api::Front::V2
  class ProjectsSearchController < Api::Front::V1::ApiController
    swagger_controller :projects_search, 'Search'

    # GET /project_search/
    swagger_api :index do
      summary 'Search'
      # Services
      param :query, :brand_service_ids, :string, :optional, "Search by brand service ids joined with ','" # Бренд
      # Geo
      param :query, :city_id, :string, :optional, 'Search by city id' # Город
      # Date
      param :query, :end_date_range, :float, :optional, "Filter by end date min and max rates joined with ','" # Год создания
      # footage
      param :query, :footage_range, :float, :optional, "Filter by footage min and max rates joined with ','" # Площадь
      # Styles
      param :query, :style_ids, :string, :optional, "Search by style ids joined with ','" # Стиль
      # Statuses
      param :query, :status_ids, :string, :optional, "Search by status ids joined with ','" # Статус проекта
      # Objects
      param :query, :object_ids, :string, :optional, "Search by object ids joined with ','" # Категории товаров
      # Objects
      param :query, :brand_ids, :string, :optional, "Search by brand ids joined with ','" # Тип обьекта
      # Pagination
      param :query, :page_number, :integer, :optional, 'Pagination page number'
      param :query, :page_size, :integer, :optional, 'Pagination page size (default 9)'

      response :unauthorized
      response :ok
    end
    def index
      projects_search = Api::ProjectsSearch.new(params: params)
      projects = projects_search.search

      projects = Kaminari.paginate_array(projects).page(params[:page_number]).per(params[:page_size] || 9)

      render json: projects, root: 'data',
             meta: meta_attributes(Project.name, projects, {}),
             adapter: :json
    end

    # GET /search/filters
    swagger_api :get_filters do
      summary 'Search filters'
      response :not_found
    end
    def get_filters
      render json: { data: all_filters }, adapter: :json_api
    end

    private
      def cities
        city_ids = Project.pluck(:city_id).compact.uniq

        City.where(id: city_ids)
      end

      def all_filters
        [
          {
              # Категории товаров
              id: :objects,
              name: I18n.t('gallery_page.filters_panel.names.objects')
          }.merge(
              ActiveModelSerializers::SerializableResource.new(
                  Catalog::Object.all,
                  each_serializer: Api::Front::V2::ProjectSearchFilterSerializer,
                  adapter: :json,
                  root: 'list'
              ).as_json
          ),
          defined?(HallwayStyles::VERSION) && Settings.use_styles ? {
              # Стиль
              id: :styles,
              name: I18n.t('gallery_page.filters_panel.names.styles')
          }.merge(
              ActiveModelSerializers::SerializableResource.new(
                  Style.all,
                  each_serializer: Api::Front::V2::ProjectSearchFilterSerializer,
                  adapter: :json,
                  root: 'list'
              ).as_json
          ) : nil,
          {
              # Тип обьекта
              id: :brands,
              name: I18n.t('gallery_page.filters_panel.names.brands')
          }.merge(
              ActiveModelSerializers::SerializableResource.new(
                  Brand.all,
                  each_serializer: Api::Front::V2::ProjectSearchFilterSerializer,
                  adapter: :json,
                  root: 'list'
              ).as_json
          ),
          {
              # Бренд
              id: :brand_services,
              name: I18n.t('gallery_page.filters_panel.names.brand_services')
          }.merge(
              ActiveModelSerializers::SerializableResource.new(
                  Service.brand_services,
                  each_serializer: Api::Front::V2::ProjectSearchFilterSerializer,
                  adapter: :json,
                  root: 'list'
              ).as_json
          ),
          {
              # Статус проекта
              id: :project_statuses,
              name: I18n.t('gallery_page.filters_panel.names.project_statuses')
          }.merge(
              ActiveModelSerializers::SerializableResource.new(
                 ProjectStatus.all,
                  each_serializer: Api::Front::V2::ProjectSearchFilterSerializer,
                  adapter: :json,
                  root: 'list'
              ).as_json
          ),
          {
              # Город
              id: :cities,
              name: I18n.t('gallery_page.filters_panel.names.cities')
          }.merge(
              ActiveModelSerializers::SerializableResource.new(
                  cities,
                  each_serializer: Api::Front::V2::ProjectSearchFilterSerializer,
                  adapter: :json,
                  root: 'list'
              ).as_json
          ),
          {
              # Год создания
              id: :end_date,
              name: I18n.t('gallery_page.filters_panel.names.end_date'),
              min: StableProject.minimum(:end_date).try(:year).to_i,
              max: StableProject.maximum(:end_date).try(:year).to_i
          },
          {
              # Площадь
              id: :footage,
              name: I18n.t('gallery_page.filters_panel.names.footage'),
              min: StableProject.minimum(:footage),
              max: StableProject.maximum(:footage)
          }
        ].compact.reject do |x|
          x.key?(:list) && x.dig(:list).empty? || x.key?(:min) && x.key?(:max) && x.dig(:min).zero? && x.dig(:max).zero?
        end
      end
  end
end
