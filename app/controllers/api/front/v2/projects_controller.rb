# frozen_string_literal: true

module Api::Front::V2
  class ProjectsController < Api::Front::V1::ApiController
    before_action :find_project, only: [:show]

    swagger_controller :projects, 'Projects management'

    # GET /projects
    swagger_api :index do
      summary 'Fetches all Projects'
      # Pagination
      param :query, :page_number, :integer, :optional, 'Pagination page number'
      param :query, :page_size, :integer, :optional, 'Pagination page size (default 9)'
      response :ok
    end
    def index
      @projects = Kaminari.paginate_array(StableProject.confirmed).page(params[:page_number]).per(params[:page_size] || 9)

      render json: @projects, root: 'data',
             meta: meta_attributes(Project.name, @projects, {}),
             adapter: :json
    end

    # GET /projects/1
    swagger_api :show do
      summary 'Fetches Project'
      param :path, :id, :integer, :required, 'Projectg ID'
      response :not_found
      response :ok
    end
    def show
      render json: @project, serializer: Api::Front::V2::ProjectSerializer, root: 'data', adapter: :json
    end

    private

    def find_project
      @project = Project.for_visible_stations.find_by(slug: params[:id])&.stable_or_last_version
      raise ActiveRecord::RecordNotFound unless @project
    end
  end
end
