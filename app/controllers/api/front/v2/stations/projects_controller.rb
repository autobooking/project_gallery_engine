# frozen_string_literal: true

module Api::Front::V2::Stations
  class ProjectsController < Api::Front::V1::ApiController
    before_action :find_station, only: [:index]

    swagger_controller :stations, 'Station Projects'

    # GET /projects
    swagger_api :index do
      summary 'Fetches all Station Projects'
      # Pagination
      param :path, :station_id, :integer, :required, 'Station ID'
      param :query, :page_number, :integer, :optional, 'Pagination page number'
      param :query, :page_size, :integer, :optional, 'Pagination page size (default 9)'
      response :ok
    end
    def index
      @projects = Kaminari.paginate_array(@station.stable_projects.confirmed).page(params[:page_number]).per(params[:page_size] || 9)

      render json: @projects, root: 'data',
             each_serializer: Api::Front::V2::StableProjectSerializer,
             meta: meta_attributes(Project.name, @projects, {}),
             adapter: :json
    end

    private

    def find_station
      @station = Station.find(params[:station_id])
    end
  end
end
