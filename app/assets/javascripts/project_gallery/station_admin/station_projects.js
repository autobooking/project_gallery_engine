/*global jQuery $*/
jQuery(document).ready(function() {
  $('#station_projects_datatable').dataTable({
    processing: true,
    serverSide: true,
    pageLength: 25,
    lengthChange: false,
    ajax: {
      url: $('#station_projects_datatable').data('source')
    },
    columns: [
      { data: 'id' },
      { data: 'name' },
      { data: 'view', searchable: false, orderable: false },
      { data: 'edit', searchable: false, orderable: false },
      { data: 'delete', searchable: false, orderable: false }
    ]
  })
})
