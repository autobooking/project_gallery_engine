/*global jQuery $*/
jQuery(document).ready(function() {
  $('#projects_datatable').dataTable({
    processing: true,
    serverSide: true,
    pageLength: 25,
    lengthChange: false,
    ajax: {
      url: $('#projects_datatable').data('source')
    },
    columns: [
      { data: 'id' },
      { data: 'name' },
      { data: 'in_my_country', searchable: false, orderable: false },
      { data: 'view', searchable: false, orderable: false },
      { data: 'edit', searchable: false, orderable: false },
      { data: 'delete', searchable: false, orderable: false }
    ]
  })
})
