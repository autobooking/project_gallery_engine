/*global $ I18n tmpl*/
$(document).ready(function() {
  $.widget('blueimp.fileupload', $.blueimp.fileupload, {
    options: {
      messages: {
        maxNumberOfFiles: I18n.t('fileupload.js.max_number_of_files'),
        acceptFileTypes: I18n.t('fileupload.js.accept_file_types'),
        maxFileSize: I18n.t('fileupload.js.max_file_size'),
        minFileSize: I18n.t('fileupload.js.min_file_size')
      }
    }
  })
  
  var $projectPhotoUploader = $('#project_photo_uploader')
  $projectPhotoUploader.parent().find('.gallery-wrap').sortable({
    tolerance: 'pointer',
    forceHelperSize: true,
    update: function() {
      let $self = $(this),
        station_id = $('#project_station_id').val()
      return $.ajax({
        url: `/station_admin/stations/${station_id}/project_photos/sort`,
        type: 'POST',
        data: $self.sortable('serialize')
      })
    }
  })
  $projectPhotoUploader.parent().find('.gallery-wrap [data-toggle="collapse"]').click(function() {
    $('.gallery-wrap .collapse.in').collapse('hide')
  })
  $projectPhotoUploader.parent().find('#dropzone').on('click', function() {
    $projectPhotoUploader.trigger('click')
  })
  $projectPhotoUploader
    .fileupload({
      dropZone: $('#dropzone'),
      dataType: 'script',
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
      maxFileSize: 20000000,
      maxTotalFileSize: 200000000,
      maxNumberOfFiles: 20,
      getNumberOfFiles: function() {
        return $('input[name="project[project_photo_ids][]"]').size()
      },
      formData: function(form) {
        return $.grep(form.serializeArray(), function(obj) {
          return (
            $.inArray(obj.name, [
              'utf8',
              'authenticity_token',
              'project_photo[imageable_id]',
              'project_photo[imageable_type]'
            ]) != -1
          )
        })
      }
    })
    .on('fileuploadprocessalways', function(e, data) {
      var index = data.index,
        file = data.files[index],
        node = $(data.context.children()[index])
      if (file.preview) {
        node.find('.preview').append(file.preview)
      }
      if (file.error) {
        node
          .find('label')
          .removeClass('label-primary')
          .addClass('label-danger')
          .find('.text')
          .append(' - ' + file.error + '&nbsp;')

        node.find('.fa-spinner').addClass('hidden')

        node.find('.fa-times').removeClass('hidden')

        node.find('.progress').removeClass('active')

        setTimeout(function() {
          node.slideUp('slow')
        }, 7000)
      }
    })
    .on('fileuploadadd', function(e, data) {
      data.context = $('<div class="row"/>').appendTo('#upload-progress')
      $.each(data.files, function(index, file) {
        $(tmpl('template-upload', file)).appendTo(data.context)
      })
    })
    .on('fileuploadprogress', function(e, data) {
      if (data.context) {
        var progress = parseInt((data.loaded / data.total) * 100, 10)
        data.context.find('.progress-bar').css('width', progress + '%')
      }
    })
    .on('fileuploaddone', function(e, data) {
      $.each(data.files, function(index) {
        var context = $(data.context.children()[index])
        context.find('.fa-check').removeClass('hidden')
      })
    })
    .on('fileuploadfail', function(e, data) {
      $.each(data.files, function(index) {
        var context = $(data.context.children()[index])
        context
          .find('label')
          .removeClass('label-primary')
          .addClass('label-danger')

        context
          .find('.progress-bar')
          .removeClass('progress-bar-success')
          .addClass('progress-bar-danger')

        context.find('.fa-times').removeClass('hidden')
      })
    })
    .on('fileuploadalways', function(e, data) {
      $.each(data.files, function(index) {
        var context = $(data.context.children()[index])
        context.find('.progress').removeClass('active')

        context.find('.fa-spinner').addClass('hidden')

        setTimeout(function() {
          context.slideUp('slow')
        }, 2000)
      })
    })
})