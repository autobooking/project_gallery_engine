/*global jQuery $ I18n moment gon*/
jQuery(document).ready(function() {
  $('.project-form input.form-date').datetimepicker({
    format: I18n.t('date.js.eu_date'),
    extraFormats: ['YYYY-MM-DD'],
    useStrict: true,
    sideBySide: true,
    allowInputToggle: true,
    icons: {
      time: 'fa fa-clock-o',
      date: 'fa fa-calendar',
      up: 'fa fa-arrow-up',
      down: 'fa fa-arrow-down',
      previous: 'fa fa-arrow-left',
      next: 'fa fa-arrow-right'
    },
    locale: gon.locale == 'ua' ? 'uk' : gon.locale || 'ru'
  })
})

