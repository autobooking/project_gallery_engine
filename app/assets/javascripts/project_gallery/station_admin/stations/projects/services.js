/*global jQuery $*/
jQuery(document).ready(function() {
  var destroy_brand_service_ids = []

  $('#projectBrandServicesForm').on('change', '.brand-service-checkbox', function() {
    if (!$(this).prop('checked')) {
      destroy_brand_service_ids.push($(this).val())

      $('#destroy_brand_service_ids').val(destroy_brand_service_ids)
    }
  })
})
