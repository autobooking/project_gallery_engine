# frozen_string_literal: true

AdditionalService.class_eval do
  include Translatable

  has_many :project_additional_services, dependent: :delete_all
  has_many :projects, through: :project_additional_services

  scope :with_translations_by_order_id, -> { includes(:translations).order(:id) }
end
