# frozen_string_literal: true

Brand.class_eval do
  include Translatable

  has_one :project
end
