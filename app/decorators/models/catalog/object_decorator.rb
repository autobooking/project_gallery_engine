# frozen_string_literal: true

Catalog::Object.class_eval do
  include Translatable
end
