# frozen_string_literal: true

Service.class_eval do
  include Translatable

  def self.brand_services
    joins('INNER JOIN service_categories ON service_categories.id = services.service_category_id '\
          'AND service_categories.brand = TRUE')
  end
end
