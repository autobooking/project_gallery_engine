# frozen_string_literal: true

Style.class_eval do
  include Translatable
end
