# frozen_string_literal: true

Station.class_eval do
  has_many :projects, dependent: :destroy
  has_many :stable_projects
end
