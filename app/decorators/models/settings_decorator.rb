# frozen_string_literal: true

Settings.class_eval do
  field :use_portfolio_for_stations, type: :boolean, default: false
end
