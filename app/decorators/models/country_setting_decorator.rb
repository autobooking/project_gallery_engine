# frozen_string_literal: true

CountrySetting.class_eval do
  scope :with_default_code, -> { where(country_code: 'default') }

  def self.iso_codes
    ISO3166::Country.all_names_with_codes(I18n.locale).tap do |country_names_with_codes|
      country_names_with_codes << default_code_option if CountrySetting.with_default_code.any?
    end
  end

  private

  def default_code_option
    ['Default', 'DEFAULT']
  end
end
