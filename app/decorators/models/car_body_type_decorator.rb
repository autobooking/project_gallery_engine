# frozen_string_literal: true

CarBodyType.class_eval do
  include Translatable

  has_one :project
end
