# frozen_string_literal: true

module StationAdmin
  class StationProjectDatatable < AjaxDatatablesRails::ActiveRecord
    extend Forwardable

    def_delegator :@view, :link_to
    def_delegator :@view, :edit_station_admin_stations_station_project_path
    def_delegator :@view, :station_admin_stations_station_project_path
    def_delegator :@view, :project_path

    def initialize(params, options = {})
      @view = options[:view_context]
      @station = options[:station]
      super
    end

    def view_columns
      @view_columns ||= {
        id: { source: 'Project.id' },
        name: { source: 'Project.name' },
        view: { searchable: false, orderable: false },
        edit: { searchable: false, orderable: false },
        delete: { searchable: false, orderable: false }
      }
    end

    def data
      records.map do |record|
        stable_record = record.stable_or_last_version

        {
          id: record.id,
          name: stable_record.name,
          view: view(record),
          edit: edit(record),
          delete: delete(record)
        }
      end
    end

    def get_raw_records
      @station.projects
    end

    private

    def edit(record)
      link_to(I18n.t('station_admin.stations.projects.index.edit', scope: :project_gallery),
              edit_station_admin_stations_station_project_path(@station, record))
    end

    def view(record)
      link_to(I18n.t('station_admin.projects.index.view', scope: :project_gallery),
              # project_path(record),
              # Replace with path helper
              "/#{I18n.locale}-#{record.country_setting.country_code.downcase}/project/#{record.slug}",
              target: "_blank")
    end

    def delete(record)
      link_to(I18n.t('station_admin.stations.projects.index.remove', scope: :project_gallery),
              station_admin_stations_station_project_path(@station, record),
              method: :delete,
              data: { confirm: I18n.t('station_admin.stations.projects.index.confirm_remove', scope: :project_gallery) })
    end
  end
end
