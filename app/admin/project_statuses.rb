ActiveAdmin.register ProjectStatus do

  permit_params :translatable_columns_values

  form do |f|
    f.inputs 'Project status' do
      f.input :translatable_columns_values, as: :json, label: 'name'
    end

    f.actions
  end

end
