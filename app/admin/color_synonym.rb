# frozen_string_literal: true

require_relative '../models/color_synonym'

if defined?(ActiveAdmin)
  ActiveAdmin.register ColorSynonym do
    menu parent: 'colors', if: proc {
      Settings.use_portfolio_for_stations &&
        current_admin.can?(:manage, ColorSynonym)
    }

    config.filters = false

    permit_params :color_id, :translatable_columns_values

    show do
      attributes_table do
        row :id
        row :name
        row :created_at
        row :updated_at
      end

      panel 'Translations' do
        table_for resource.translations do
          column :locale
          column :name
        end
      end
    end

    form do |f|
      f.inputs 'ColorSynonym Details' do
        f.input :color, as: :select
      end
      f.inputs 'Translations' do
        f.input :translatable_columns_values, as: :json, label: 'Name'
      end
      f.actions
    end
  end
end
