# frozen_string_literal: true

require_relative '../models/color'

if defined?(ActiveAdmin)
  ActiveAdmin.register Color do
    menu parent: 'colors', if: proc {
      Settings.use_portfolio_for_stations &&
        current_admin.can?(:manage, Color)
    }

    config.filters = false

    permit_params :translatable_columns_values, :image, permitted_countries: []

    index do
      selectable_column
      id_column
      column :image do |color|
        image_tag color.image_url(:small) if color.image_url(:small)
      end
      column :name
      column :slug
      column :created_at
      actions
    end

    show do
      attributes_table do
        row :id
        row :name
        row :slug
        row :permitted_countries
        row :image do |color|
          image_tag color.image_url(:small) if color.image_url(:small)
        end
        row :created_at
        row :updated_at
      end

      panel 'Translations' do
        table_for resource.translations do
          column :locale
          column :name
          column :slug
        end
      end
    end

    form do |f|
      f.inputs 'Color Details' do
        f.input :image, hint: (image_tag f.object.image_url(:small) if f.object.image_url(:small))
        f.input :permitted_countries,
                as: :select,
                multiple: true,
                collection: AdminPanel::PermittedCountries.new(f.object, current_admin).countries,
                include_hidden: false
        f.input :translatable_columns_values, as: :json, label: 'Name'
      end

      f.actions
    end
  end
end
