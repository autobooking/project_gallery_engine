# frozen_string_literal: true

require_relative '../models/project_status'

if defined?(ActiveAdmin)
  ActiveAdmin.register ProjectStatus do

    permit_params :translatable_columns_values

    index download_links: false do
      selectable_column
      id_column
      column :name
      actions
    end

    form do |f|
      f.inputs 'Project status' do
        f.input :translatable_columns_values, as: :hstore, label: 'name'
      end

      f.actions
    end
  end
end
