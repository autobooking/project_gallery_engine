require_relative '../models/project_mode'

if defined?(ActiveAdmin)
  ActiveAdmin.register ProjectMode do
    menu parent: 'station_settings'

    permit_params :translatable_columns_values

    index do
      selectable_column
      id_column
      column :name
      column :description
      actions
    end

    csv force_quotes: true, col_sep: ',' do
      column :id, humanize_name: false

      [:name, :description].each do |attr_name|
        I18n.available_locales.each do |locale|
          column("#{attr_name}_#{locale.to_s.underscore}", humanize_name: false) { |marker| marker.translation_for(locale).send("#{attr_name}") }
        end
      end

      column :updated_at, humanize_name: false
    end

    filter :translations_name, as: :string
    filter :description
    filter :created_at
    filter :updated_at

    form do |f|
      f.inputs I18n.t('active_admin.resources.station_mode.inputs.details') do
        f.input :translatable_columns_values, as: :json
      end
      f.actions
    end

  end
end
