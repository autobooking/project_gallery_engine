# frozen_string_literal: true

require_relative '../models/project'

if defined?(ActiveAdmin)
  ActiveAdmin.register Project do
    belongs_to :station, optional: true

    config.filters = false

    permit_params :name, :description, :region, :country, :address_real, :photographer, :footage, :end_date,
                  :latitude, :longitude, :brand_id, :style_id, :color_id, :car_body_type_id, :project_status_id,
                  :cover_crop_x, :cover_crop_y, :cover_crop_w, :cover_crop_h,
                  additional_service_ids: [],
                  meta_data_attributes: [:title, :keywords, :description],
                  project_object_services_attributes: [:project_id, :object_id, :service_id],
                  project_photos_attributes: [:id, :image],
                  project_videos_attributes: [:id, :link]

    index download_links: false do
      selectable_column
      id_column
      column :name
      column :likes_count
      actions
    end

    form do |f|
      f.inputs 'Details' do
        f.input :name
        f.input :description
        f.input :photographer
        f.input :style,
                as: :select,
                collection: Style.with_preload_translations if defined?(HallwayStyles::VERSION) && Settings.use_styles
        f.input :brand,
                as: :select,
                collection: Brand.with_preload_translations
        f.input :color,
                as: :select,
                collection: Color.with_preload_translations
        f.input :car_body_type, as: :select
        f.input :additional_services,
                collection: AdditionalService.with_preload_translations,
                as: :select, multiple: true
        f.input :project_status,
                as: :select,
                collection: ProjectStatus.with_preload_translations
        f.input :footage
        f.input :end_date, as: :datepicker
      end

      f.inputs 'Position' do
        f.input :city,
                as: :select,
                collection: cities_by_admin_role(I18n.locale)
        f.input :region
        f.input :country,
                as: :select,
                collection: CountrySetting.all.map { |country| [country.name, country.id] }
        f.input :latitude
        f.input :longitude
      end

      f.inputs 'Metadata', for: [:meta_data, project.meta_data || MetaData.new] do |ff|
        ff.input :title
        ff.input :keywords
        ff.input :description
      end

      f.has_many :project_photos, allow_destroy: true do |ff|
        ff.input :image, hint: (image_tag(ff.object.image.url(:medium_project_photo)) if ff.object.image?)
      end

      f.has_many :project_videos, allow_destroy: true do |ff|
        ff.input :link
      end

      f.has_many :project_object_services, allow_destroy: true do |ff|
        ff.input :object,
                 as: :select,
                 collection: Catalog::Object.with_preload_translations
        ff.input :service,
                 as: :select,
                 collection: Service.brand_services
      end

      f.actions
    end
  end
end
