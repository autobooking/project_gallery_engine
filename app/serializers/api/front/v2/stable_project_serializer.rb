class Api::Front::V2::StableProjectSerializer < Api::Front::V2::ProjectSerializer
  def cover
    cover = object.project.stable_or_last_version.cover
    if cover.url
      {
          origin_url: cover.url,
          desktop_url: cover.desktop_project_cover.url,
          mobile_url: cover.mobile_project_cover.url
      }
    else
      first_project_photo = object.project.project_photos.first.try(:image)

      return {} unless first_project_photo

      {
          origin_url: first_project_photo.url,
          desktop_url: first_project_photo.desktop_project_photo.url,
          mobile_url: first_project_photo.mobile_project_photo.url
      }
    end
  end

  def project_objects
    ActiveModel::Serializer::CollectionSerializer.new(
        object.stable_project_object_services,
        serializer: Api::Front::V2::ProjectObjectServiceSerializer,
        adapter: :json, root: false
    ).as_json
  end

  def photos
    ActiveModel::Serializer::CollectionSerializer.new(
        object.project.stable_or_last_version.project_photos,
        serializer: Api::Front::V2::ProjectPhotoSerializer,
        adapter: :json,
        root: false
    ).as_json
  end

  def project_videos
    ActiveModel::Serializer::CollectionSerializer.new(
        object.project.project_videos,
        serializer: Api::Front::V2::ProjectVideoSerializer,
        adapter: :json, root: false
    ).reject { |x| x.youtube_id.blank? }.as_json
  end
end
