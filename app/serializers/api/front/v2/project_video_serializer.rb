require('youtube_addy')

class Api::Front::V2::ProjectVideoSerializer < Api::Front::V1::BaseSerializer
  attributes :youtube_id

  def youtube_id
    ::YouTubeAddy.extract_video_id(object.link)
  end
end