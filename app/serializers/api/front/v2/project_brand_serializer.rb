# frozen_string_literal: true

class Api::Front::V2::ProjectBrandSerializer < Api::Front::V1::BaseSerializer
  attributes :id, :name
end
