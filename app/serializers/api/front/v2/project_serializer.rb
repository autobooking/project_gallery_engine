class Api::Front::V2::ProjectSerializer < Api::Front::V1::BaseSerializer
  attributes :id, :slug, :name, :city, :style, :status, :color, :description, :views_count, :likes_count,
             :footage, :photographer, :brand, :station_id, :brand_services, :photos, :project_videos,
             :project_objects, :station, :cover

  def cover
    if object.cover.url
      {
        origin_url: object.cover.url,
        desktop_url: object.cover.desktop_project_cover.url,
        mobile_url: object.cover.mobile_project_cover.url
      }
    else
      first_project_photo = object.project_photos.first.try(:image)

      return {origin_url: nil, desktop_url: nil,  mobile_url: nil} unless first_project_photo.present?

      {
        origin_url: first_project_photo.url,
        desktop_url: first_project_photo.desktop_project_photo.url,
        mobile_url: first_project_photo.mobile_project_photo.url
      }
    end
  end

  def city
    ActiveModelSerializers::SerializableResource.new(
        object.city,
        serializer: Api::Front::V2::CitySerializer,
        adapter: :attributes
    ).as_json
  end

  def style
    return {} unless defined?(HallwayStyles::VERSION) && Settings.use_styles

    ActiveModelSerializers::SerializableResource.new(
        object.style,
        serializer: Api::Front::V2::StationStyleSerializer,
        adapter: :attributes
    ).as_json
  end

  def status
    object.project_status.try(:name)
  end

  def color
    object.color.try(:name)
  end

  def brand
    ActiveModelSerializers::SerializableResource.new(
        object.brand,
        serializer: Api::Front::V2::BrandSerializer,
        adapter: :attributes
    ).as_json
  end

  def brand_services
    ActiveModel::Serializer::CollectionSerializer.new(
        object.services,
        serializer: Api::Mobile::V1::ServiceSerializer,
        adapter: :json, root: false
    ).as_json
  end

  def station
    station = object.station

    return {} unless station

    {
      id: station.id,
      marker_slug: station.try(:marker).try(:slug),
      marker_name: station.try(:marker).try(:name),
      city_slug: station.try(:city).try(:slug),
      slug: station.slug,
      name: station.name,
      cover_url: station.cover_url(:small_square_station_cover)
    }
  end

  def project_objects
    ActiveModel::Serializer::CollectionSerializer.new(
        object.project_object_services,
        serializer: Api::Front::V2::ProjectObjectServiceSerializer,
        adapter: :json, root: false
    ).as_json
  end

  def project_videos
    ActiveModel::Serializer::CollectionSerializer.new(
        object.project_videos,
        serializer: Api::Front::V2::ProjectVideoSerializer,
        adapter: :json, root: false
    ).reject { |x| x.youtube_id.blank? }.as_json
  end

  def photos
    ActiveModel::Serializer::CollectionSerializer.new(
        object.project_photos,
        serializer: Api::Front::V2::ProjectPhotoSerializer,
        adapter: :json,
        root: false
    ).as_json
  end
end
