class Api::Front::V2::ProjectObjectServiceSerializer < Api::Front::V1::BaseSerializer
  attributes :name, :category

  def name
    return unless object.object

    object.object.name
  end

  def category
    return unless object.object && object.object.subcategory

    {
      name: object.object.subcategory.name,
      id: object.object.subcategory.id
    }
  end
end