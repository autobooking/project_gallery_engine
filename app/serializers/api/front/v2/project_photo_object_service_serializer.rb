class Api::Front::V2::ProjectPhotoObjectServiceSerializer < Api::Front::V1::BaseSerializer
  attributes :brand_service, :interior_object

  def brand_service
    service = object.service

    { id: service.id, name: service.name}
  end

  def interior_object
    interior_object = object.object
    
    { id: interior_object.id, name: interior_object.name}
  end
end
