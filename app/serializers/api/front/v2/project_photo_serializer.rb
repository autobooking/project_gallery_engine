class Api::Front::V2::ProjectPhotoSerializer < Api::Front::V1::BaseSerializer
  attributes :id, :alt, :description, :name, :origin_url, :thumb_url, :desktop_url, :mobile_url, :big_url, :medium_url,
             :object_services, :style, :brand

  def origin_url
    object.image_url.to_s
  end

  def thumb_url
    object.image.thumb.url
  end

  def desktop_url
    object.image.desktop_project_photo.url
  end

  def mobile_url
    object.image.mobile_project_photo.url
  end

  def big_url
    object.image.big_project_photo.url
  end

  def medium_url
    object.image.medium_project_photo.url
  end

  def object_services
    ActiveModel::Serializer::CollectionSerializer.new(
        object.project_photo_object_services,
        serializer: Api::Front::V2::ProjectPhotoObjectServiceSerializer,
        adapter: :json, root: false
    ).as_json
  end

  def style
    return {} unless defined?(HallwayStyles::VERSION) && Settings.use_styles
    
    ActiveModelSerializers::SerializableResource.new(
        object.style,
        each_serializer: Api::Front::V2::ProjectStyleSerializer,
        adapter: :attributes
    ).as_json
  end

  def brand
    ActiveModelSerializers::SerializableResource.new(
        object.brand,
        each_serializer: Api::Front::V2::ProjectBrandSerializer,
        adapter: :attributes
    ).as_json
  end
end
