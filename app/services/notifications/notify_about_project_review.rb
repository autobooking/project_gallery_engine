# frozen_string_literal: true

module Notifications
  class NotifyAboutProjectReview
    def initialize(project:, code:)
      @project = project
      @code = code
    end

    def call
      event = NotificationEvent.find_by(code: code)

      return unless event

      recipients_list = []

      recipients_list << recipients_station_admins

      recipients_list << recipients_station_operators

      recipients_list << recipients_station_manager

      event.true_deliver(
        recipients_list: recipients_list.flatten,
        country_code: project.country_setting.country_code,
        params: { project: project }
      )
    end

    private

    attr_reader :project, :code

    def recipients_station_admins
      project
        .station_admins
        .map(&:to_notification_param)
    end

    def recipients_station_operators
      User
        .operator
        .where(country_setting: project.country_setting)
        .map(&:to_notification_param)
    end

    def recipients_station_manager
      station_manager = project.station_manager
      return User.none unless station_manager.present?

      station_manager.to_notification_param
    end
  end
end
