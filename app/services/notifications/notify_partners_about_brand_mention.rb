# frozen_string_literal: true

module Notifications
  class NotifyPartnersAboutBrandMention
    def call(project:)
      event = NotificationEvent.find_by(code: NotificationEvent::PROJECT_REVIEW_APPROVED)

      brand_services_ids = project.services.pluck(:id)

      recipients = User.joins(managed_stations: [:sto_mode, :services])
                       .includes(:auth_tokens)
                       .where("sto_modes.code = 'partner'")
                       .where("services.id IN (?)", brand_services_ids)
                       .distinct
                       .map(&:to_notification_param)

      brand_services = Service.joins(:stations)
                              .joins("INNER JOIN managers_stations ON managers_stations.station_id = stations.id")
                              .joins("INNER JOIN users on users.id = managers_stations.user_id")
                              .where("services.id IN (?)", brand_services_ids)
                              .where("users.id IN (?)", recipients.pluck(:id))
                              .distinct

      event.true_deliver(
        recipients_list: recipients,
        country_code: project.country_setting.country_code,
        params: { brand_services: brand_services, project: project }
      )
    end
  end
end
