# frozen_string_literal: true

class ProjectVersionReview
  attr_reader :project, :params, :commit

  def initialize(project, commit, params = {})
    @project = project
    @params = params
    @commit = commit
  end

  def call
    set_paper_trail_event

    if canceled?
      revert_changes
    else
      project.assign_attributes(params.merge(need_moderation: false, project_mode_id: ProjectMode::ACTIVE_ID))

      project.paper_trail.save_with_version

      project.update_stable_project
    end

    clear_versions

    Notifications::NotifyAboutProjectReview.new(project: project, code: code).call
  end

  private

    def canceled?
      commit == 'cancel'
    end

    def approved?
      commit == 'approve'
    end

    def revert_changes
      if project.confirmed?
        return unless project.stable_version

        project.stable_version.reify(has_many: true).save
      else
        project.update(project_mode_id: ProjectMode::CANCELED_ID, need_moderation: false)
      end
    end

    def clear_versions
      stable_version_id = project.stable_version.id
      project.version_associations.destroy_all
      project.versions.where.not(id: stable_version_id).destroy_all
    end

    def code
      if canceled?
        NotificationEvent::PROJECT_REVIEW_CANCELLED
      elsif approved?
        NotificationEvent::PROJECT_REVIEW_APPROVED
      end
    end

    def set_paper_trail_event
      project.paper_trail_event = Project::STABLE_VERSION_EVENT
    end
end
