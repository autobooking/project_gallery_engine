# frozen_string_literal: true

module Api
  class ProjectsSearch
    def initialize(params:)
      @params = params

      @brand_service_ids = params[:brand_service_ids].to_s.split(',')     # Бренд
      @end_date_range = params[:end_date_range].to_s.split(',')           # Год создания
      @footage_range = params[:footage_range].to_s.split(',')             # Площадь
      @city_id = params[:city_id]                                                # Город
      @style_ids = params[:style_ids].to_s.split(',')                     # Стиль
      @status_ids = params[:status_ids].to_s.split(',')                   # Статус проекта
      @object_ids = params[:object_ids].to_s.split(',')                   # Категории товаров
      @brand_ids = params[:brand_ids].to_s.split(',')                     # Тип обьекта
    end

    def search
      projects = StableProject.confirmed
      projects = by_brand_service(projects)          if @brand_service_ids.present?  # Бренд
      projects = by_end_date_range(projects)         if @end_date_range.present?     # Год создания
      projects = by_footage_range(projects)          if @footage_range.present?      # Площадь
      projects = by_city_id(projects)                if @city_id.present?            # Город
      projects = by_style_ids(projects)              if @style_ids.present?          # Стиль
      projects = by_status_ids(projects)             if @status_ids.present?         # Статус проекта
      projects = by_object_ids(projects)             if @object_ids.present?         # Категории товаров
      projects = by_brand_ids(projects)              if @brand_ids.present?          # Тип обьекта
      # Если карточка бизнеса в котором создан проект в режиме Лид, Партнёр, Чёрный список, Отказники, Выключено, то Карточка проекта не отображается в галерее
      projects = projects.for_visible_stations                                        

      projects
    end

    private
      def by_brand_service(projects)
        projects.joins(:stable_project_object_services).distinct.where(stable_project_object_services: { service_id: @brand_service_ids })
      end

      def by_end_date_range(projects)
        projects.where('end_date >= ? AND end_date <= ?', DateTime.new(@end_date_range.first.to_i), DateTime.new(@end_date_range.last.to_i).end_of_year)
      end

      def by_footage_range(projects)
        projects.where('footage >= ? AND footage <= ?', @footage_range.first.to_i, @footage_range.last.to_i)
      end

      def by_city_id(projects)
        projects.where(city_id: @city_id)
      end

      def by_style_ids(projects)
        return projects unless defined?(HallwayStyles::VERSION) && Settings.use_styles
        projects.where(style_id: @style_ids)
      end

      def by_status_ids(projects)
        projects.where(project_status_id: @status_ids)
      end

      def by_object_ids(projects)
        projects.joins(:stable_project_object_services).distinct.where(stable_project_object_services: { object_id: @object_ids })
      end

      def by_brand_ids(projects)
        projects.where(brand_id: @brand_ids)
      end
  end
end
