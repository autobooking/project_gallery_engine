module ProjectHelper
  def edit_url
    station_admin_stations_station_project_path(@station, @project)
  end

  def version_diff(object = @project, column)
    return unless current_user.operator? || current_user.station_manager?

    return unless params[:moderation] == 'true'


    if object.is_a?(ProjectPhoto) || object.is_a?(ProjectObjectService)
      return unless object.project.need_moderation && object.changed?

      'background-color: #f2dede;'.html_safe
    else
      return unless object.need_moderation && object.column_changed?(column)

      'bg-danger'.html_safe
    end
  end

  def awaiting_review_projects
    @awaiting_review_projects ||= Project.need_moderation
  end

  def add_button(name)
    "<button class='btn btn-default' type='button'>"\
    "#{name}"\
    "</button>".html_safe
  end

  def xs_add_button(name)
    "<button class='btn btn-default btn-xs' type='button'>"\
    "#{name}"\
    "</button>".html_safe
  end

  def remove_button(name)
    "<button class='btn btn-danger' type='button'>"\
    "#{name}"\
    "</button>".html_safe
  end

  def xs_remove_button(name)
    "<button class='btn btn-danger btn-xs' type='button'>"\
    "#{name}"\
    "</button>".html_safe
  end

  def truncate_body(description, photo)
    HTML_Truncator.truncate(description, photo ? 10 : 75, ellipsis: '').html_safe
  end

  def back_to_list_path
    if current_user.station_admin?
      station_admin_stations_station_projects_path(@station)
    elsif current_user.operator?
      station_admin_projects_path
    end
  end
end
