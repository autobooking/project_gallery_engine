# frozen_string_literal: true

if !Object.const_defined?('Station')
  class Station < ApplicationRecord; end
end
