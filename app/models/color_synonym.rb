# frozen_string_literal: true

require 'globalize'

class ColorSynonym < ApplicationRecord
  include TranslationsConcern

  translates :name

  belongs_to :color

  validates :color, presence: true
end
