# frozen_string_literal: true

class ProjectObjectService < ApplicationRecord
  include Versionable

  has_paper_trail

  belongs_to :project

  belongs_to :object, class_name: 'Catalog::Object', optional: true

  belongs_to :service, optional: true

  def changed?
    stable_version = project.stable_version
    
    return true unless stable_version
    
    reified_stable_version = stable_version.reify(has_many: true)
    
    return false unless reified_stable_version
    
    stable_assoc = reified_stable_version.project_object_services

    stable_attributes = stable_assoc.select{|object_service| object_service.id == id}.first.try(:attributes)

    attributes != stable_attributes
  end
end
