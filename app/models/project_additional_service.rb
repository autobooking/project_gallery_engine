# frozen_string_literal: true

class ProjectAdditionalService < ApplicationRecord
  belongs_to :project

  belongs_to :additional_service
end
