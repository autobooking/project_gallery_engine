# frozen_string_literal: true

module Versionable
  extend ActiveSupport::Concern

  included do
    before_destroy do
      versions.destroy_all
    end

    before_create do
      self.paper_trail_event = Project::STABLE_VERSION_EVENT
    end
  end

  def version_associations
    PaperTrail::Version.joins(:version_associations).where(version_associations: { foreign_key_id: id })
  end

  def column_changed?(column)
    version = draft_version
    return unless version

    version.object_changes[column].present?
  end

  def stable_version
    versions.where(event: Project::STABLE_VERSION_EVENT).last
  end

  def last_version
    versions.last
  end

  def draft_version
    versions.where.not(event: Project::STABLE_VERSION_EVENT).last
  end

  def need_moderation?
    draft_version.present?
  end

  private
    def photo?
      self.class.name == 'ProjectPhoto'
    end
end
