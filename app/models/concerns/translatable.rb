# frozen_string_literal: true

module Translatable
  extend ActiveSupport::Concern

  included do
    scope :with_preload_translations, -> { preload(:translations) }
  end
end
