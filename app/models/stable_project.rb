# frozen_string_literal: true

class StableProject < ApplicationRecord
  include NormalizeSlugConcern

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]

  def slug_candidates
    [
        :name,
        [:name, city.try(:name)],
        [:name, city.try(:name), :id]
    ]
  end

  belongs_to :project

  belongs_to :station

  belongs_to :project_mode

  belongs_to :city

  belongs_to :district

  belongs_to :brand

  belongs_to :car_body_type

  belongs_to :color

  belongs_to :style

  belongs_to :project_status

  has_one :country_setting, through: :station

  has_one :station_manager, through: :station

  has_many :station_admins, through: :station

  has_many :stable_project_object_services, dependent: :delete_all

  has_many :services, -> { distinct }, through: :stable_project_object_services

  has_many :objects, through: :stable_project_object_services

  scope :confirmed, -> { where(project_mode_id: ProjectMode::ACTIVE_ID) }

  scope :for_visible_stations, -> { joins(station: :sto_mode).where(stations: { sto_modes: { code: Project::VISIBLE_STO_MODES } }) }

  before_save :normalize_slug

  after_commit :generate_slug, on: :create

  def create_stable_project_object_service(object_service)
    stable_project_object_services.create(object_service.attributes.except('project_id', 'id'))
  end

  private
    def generate_slug
      save! if should_generate_new_friendly_id?
    end
end
