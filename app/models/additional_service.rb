# frozen_string_literal: true

if !Object.const_defined?('AdditionalService')
  class AdditionalService < ApplicationRecord
  end
end
