# frozen_string_literal: true

class MetaData < ApplicationRecord
  self.table_name = 'meta_datas'

  belongs_to :project
end
