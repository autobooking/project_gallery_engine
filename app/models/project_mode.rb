class ProjectMode < ApplicationRecord
  include TranslationsConcern

  ACTIVE_CODE = 'confirmed'.freeze
  ACTIVE_ID = 2.freeze
  CANCELED_ID = 3.freeze

  has_many :projects, dependent: :nullify
  translates :name, :description
end
