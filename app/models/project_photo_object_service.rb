# frozen_string_literal: true

class ProjectPhotoObjectService < ApplicationRecord
  belongs_to :project_photo

  belongs_to :object, class_name: 'Catalog::Object', optional: true

  belongs_to :service, optional: true
end
