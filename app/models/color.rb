# frozen_string_literal: true

require 'carrierwave'
require 'carrierwave/orm/activerecord'

class Color < ApplicationRecord
  include NormalizeSlugConcern
  include Translatable

  translates :name, :slug

  extend FriendlyId

  friendly_id :slug_candidates, use: [:slugged, :globalize]

  def slug_candidates
    [
      :name,
      [:name, :id]
    ]
  end

  include TranslationsConcern
  include ElasticsearchGlobalizeConcern

  mount_uploader :image, ColorUploader

  has_many :projects

  has_many :color_synonyms, dependent: :destroy

  accepts_nested_attributes_for :translations

  before_save :normalize_slug

  def to_param
    id&.to_s
  end

  def normalize_friendly_id(text)
    text.to_slug.normalize(
      transliterations: BabosaTransliterator.get_transliterator(
        FakeFallback.get_locale(Globalize.locale)
      )
    ).to_s.downcase.dasherize
  end
end
