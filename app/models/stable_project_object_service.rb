# frozen_string_literal: true

class StableProjectObjectService < ApplicationRecord
  belongs_to :stable_project

  belongs_to :object, class_name: 'Catalog::Object', optional: true

  belongs_to :service, optional: true
end
