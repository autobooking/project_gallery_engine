# frozen_string_literal: true

class Project < ApplicationRecord
  STABLE_VERSION_EVENT = 'stable update'.freeze
  VISIBLE_STO_MODES = ['on', 'test', 'free'].freeze

  include NormalizeSlugConcern

  include Versionable

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]

  def slug_candidates
    [
      :name,
      [:name, city.try(:name)],
      [:name, city.try(:name), :id]
    ]
  end

  has_paper_trail only: [:name, :alt, :description, :footage, :project_status_id, :style_id, :position, :cover]

  belongs_to :station, counter_cache: true

  belongs_to :project_mode

  belongs_to :city, counter_cache: true

  belongs_to :district, counter_cache: true, class_name: 'Region'

  belongs_to :brand

  belongs_to :car_body_type

  belongs_to :color

  belongs_to :style

  belongs_to :project_status

  has_one :country_setting, through: :station

  has_one :meta_data, dependent: :destroy

  has_one :stable_project, dependent: :destroy

  has_one :station_manager, through: :station

  has_many :station_admins, through: :station

  has_many :project_object_services, dependent: :delete_all

  has_many :services, -> { distinct }, through: :project_object_services

  has_many :objects, through: :project_object_services

  has_many :project_additional_services, dependent: :delete_all

  has_many :additional_services, through: :project_additional_services

  has_many :project_photos, -> { order(position: :asc) }, dependent: :destroy

  has_many :project_videos, dependent: :destroy

  accepts_nested_attributes_for :project_object_services, allow_destroy: true, reject_if: :all_blank

  accepts_nested_attributes_for :project_photos, allow_destroy: true, reject_if: :all_blank

  accepts_nested_attributes_for :project_videos, allow_destroy: true, reject_if: proc { |attributes| attributes['link'].blank? }

  accepts_nested_attributes_for :meta_data, allow_destroy: true

  scope :need_moderation, -> { where(need_moderation: true) }

  scope :confirmed, -> { where(project_mode_id: ProjectMode::ACTIVE_ID) }

  scope :with_country, ->(country_code) { joins(:country_setting).where(country_settings: { country_code: country_code }) }

  scope :for_visible_stations, -> { joins(station: :sto_mode).where(stations: { sto_modes: { code: VISIBLE_STO_MODES } }) }

  validates :slug, uniqueness: true, if: -> { slug.present? }
  validate :cover_presence

  before_save :normalize_slug, :generate_alt

  after_commit :generate_slug, on: :create

  after_create :notify_about_project_review, :create_stable_project

  AVAILABLE_PROJECT_MODES = %w[lead confirmed canceled].freeze
  AVAILABLE_PROJECT_MODES.each do |code|
    define_method "#{code}?" do
      project_mode&.code == code
    end
  end

  mount_uploader :cover, ProjectCoverUploader
  mount_base64_uploader :cover, ProjectCoverUploader

  def notify_about_project_review
    Notifications::NotifyAboutProjectReview.new(
      project: self,
      code: NotificationEvent::PROJECT_ON_REVIEW
    ).call
  end

  def normalize_friendly_id(text)
    text.to_slug.normalize(
      transliterations: BabosaTransliterator.get_transliterator(
        FakeFallback.get_locale(country_setting.try(:locale).try(:to_sym))
      )
    ).to_s.downcase.dasherize
  end

  def cover_photo
    project_photos.first.try(:image).try(:url, :medium_project_photo)
  end

  def update_stable_project
    sp = stable_project
    sp.update(attributes.except('id', 'need_moderation', 'cover'))

    sp.stable_project_object_services.destroy_all

    project_object_services.each do |object_service|
      sp.create_stable_project_object_service(object_service)
    end
  end

  def stable_or_last_version
    stable_version.try(:reify, has_many: true) || self
  end

  private
    def cover_presence
      return unless confirmed?

      return if cover.url || project_photos.present?

      errors.add :cover_image, I18n.t('project_gallery.station_admin.stations.projects.shared.project_form.errors.cover')
    end

    def generate_slug
      save! if should_generate_new_friendly_id?
    end

    def should_generate_new_friendly_id?
      slug.blank? && !new_record?
    end

    def generate_alt
      # (прим.: Отель Скандинавский Красный Синий Зелёный)
      return unless alt.blank?

      self.alt = "#{brand.try(:name)} #{style.try(:name)} #{color.try(:name)}"
    end

    def create_stable_project
      sp = create_stable_project!(attributes.except('id', 'need_moderation', 'cover'))

      project_object_services.each do |object_service|
        sp.create_stable_project_object_service(object_service)
      end
    end
end
