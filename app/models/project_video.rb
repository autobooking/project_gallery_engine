# frozen_string_literal: true

class ProjectVideo < ApplicationRecord
  belongs_to :project
end
