# frozen_string_literal: true

class ProjectPhoto < ApplicationRecord
  include Versionable

  has_paper_trail

  mount_uploader        :image, ProjectPhotoUploader
  mount_base64_uploader :image, ProjectPhotoUploader
  store_in_background   :image

  belongs_to :project
  belongs_to :color
  belongs_to :style
  belongs_to :brand

  has_many :project_photo_object_services, dependent: :delete_all
  has_many :services, -> { distinct }, through: :project_photo_object_services
  has_many :objects, through: :project_photo_object_services
  accepts_nested_attributes_for :project_photo_object_services, allow_destroy: true

  before_save :generate_alt

  def changed?
    stable_version = project.stable_version

    return true unless stable_version

    reified_stable_version = stable_version.reify(has_many: true)

    return false unless reified_stable_version

    stable_assoc = reified_stable_version.project_photos

    stable_attributes = stable_assoc.select{|photo| photo.id == id}.first.try(:attributes)

    attributes != stable_attributes
  end

  private
    def generate_alt
      # (прим.: Отель Скандинавский Зелёный)
      return unless alt.blank?

      generated_alt = "#{brand.try(:name)} #{style.try(:name)} #{color.try(:name)}"

      self.alt = generated_alt.blank? ? project.try(:alt) : generated_alt
    end
end

