# frozen_string_literal: true

if !Object.const_defined?('Service')
  class Service < ApplicationRecord
  end
end