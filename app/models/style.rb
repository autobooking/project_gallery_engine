# frozen_string_literal: true

if defined?(HallwayStyles::VERSION)
  require 'hallway_styles'
end

if !Object.const_defined?('Style')
  class Style < ApplicationRecord
  end
end
