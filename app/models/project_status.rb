class ProjectStatus < ApplicationRecord
  include TranslationsConcern
  include Translatable

  translates :name

  has_one :project

  validates :name, presence: true, uniqueness: true
end
