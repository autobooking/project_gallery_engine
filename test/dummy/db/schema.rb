# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_03_073844) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "hstore"
  enable_extension "plpgsql"

  create_table "color_synonym_translations", force: :cascade do |t|
    t.bigint "color_synonym_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["color_synonym_id"], name: "index_color_synonym_translations_on_color_synonym_id"
    t.index ["locale"], name: "index_color_synonym_translations_on_locale"
  end

  create_table "color_synonyms", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "color_id"
    t.index ["color_id"], name: "index_color_synonyms_on_color_id"
  end

  create_table "color_translations", force: :cascade do |t|
    t.bigint "color_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "slug"
    t.index ["color_id"], name: "index_color_translations_on_color_id"
    t.index ["locale"], name: "index_color_translations_on_locale"
  end

  create_table "colors", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "image"
    t.text "permitted_countries", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_colors_on_slug"
  end

  create_table "meta_datas", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.text "keywords"
    t.bigint "project_id"
    t.index ["project_id"], name: "index_meta_datas_on_project_id"
  end

  create_table "project_additional_services", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "additional_service_id", null: false
  end

  create_table "project_photos", force: :cascade do |t|
    t.string "image"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "project_id"
    t.string "image_tmp"
    t.string "alt"
    t.index ["project_id"], name: "index_project_photos_on_project_id"
  end

  create_table "project_status_translations", force: :cascade do |t|
    t.bigint "project_status_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["locale"], name: "index_project_status_translations_on_locale"
    t.index ["project_status_id"], name: "index_project_status_translations_on_project_status_id"
  end

  create_table "project_statuses", force: :cascade do |t|
    t.hstore "name"
  end

  create_table "project_videos", force: :cascade do |t|
    t.string "link"
    t.bigint "project_id"
    t.index ["project_id"], name: "index_project_videos_on_project_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "slug"
    t.string "country"
    t.string "address_real"
    t.float "latitude"
    t.float "longitude"
    t.integer "views_count"
    t.integer "likes_count", default: 0
    t.date "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "city_id"
    t.bigint "style_id"
    t.bigint "brand_id"
    t.bigint "station_id"
    t.string "region"
    t.decimal "footage", default: "0.0"
    t.bigint "project_status_id"
    t.bigint "color_id"
    t.bigint "brand_id"
    t.boolean "need_moderation", default: true
    t.string "photographer"
    t.index ["brand_id"], name: "index_projects_on_brand_id"
    t.index ["city_id"], name: "index_projects_on_city_id"
    t.index ["color_id"], name: "index_projects_on_color_id"
    t.index ["project_status_id"], name: "index_projects_on_project_status_id"
    t.index ["station_id"], name: "index_projects_on_station_id"
    t.index ["style_id"], name: "index_projects_on_style_id"
  end

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string "foreign_key_name", null: false
    t.integer "foreign_key_id"
    t.string "foreign_type"
    t.index ["foreign_key_name", "foreign_key_id", "foreign_type"], name: "index_version_associations_on_foreign_key"
    t.index ["version_id"], name: "index_version_associations_on_version_id"
  end

  add_foreign_key "color_synonyms", "colors"
  add_foreign_key "meta_datas", "projects"
  add_foreign_key "project_videos", "projects"
end
