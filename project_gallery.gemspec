$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'project_gallery/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'project_gallery'
  spec.version     = ProjectGallery::VERSION
  spec.authors     = ['kolishchak', 'kruhlyi']
  spec.email       = ['bohdan.kolishchak@gmail.com', 'maksym.kruhlyi@gmail.com']
  spec.homepage    = 'https://gitlab.com/autobooking/project_gallery_engine/'
  spec.summary     = 'Project gallery module'
  spec.description = spec.summary

  spec.files = Dir['{app,config,db,lib}/**/*', 'Rakefile', 'README.md']

  spec.add_runtime_dependency 'rails'
  spec.add_runtime_dependency 'rails-settings-cached'
  spec.add_runtime_dependency 'active_model_serializers'
  spec.add_runtime_dependency 'activeadmin'
  spec.add_runtime_dependency 'activeadmin_json_editor'
  spec.add_runtime_dependency 'activeadmin_addons'
  spec.add_runtime_dependency 'activeadmin_settings_cached'
  spec.add_runtime_dependency 'ajax-datatables-rails'
  spec.add_runtime_dependency 'carrierwave'
  spec.add_runtime_dependency 'globalize'
  spec.add_runtime_dependency 'paper_trail'
  spec.add_runtime_dependency 'youtube_addy'

  spec.add_development_dependency 'pg'
end
